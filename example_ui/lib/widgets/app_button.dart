import 'package:example_ui/example_ui.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  final ButtonStyle? style;
  AppButton.primary({Key? key, required this.text, required this.onPressed})
      : style = ElevatedButton.styleFrom(
            padding: const EdgeInsets.all(0),
            textStyle: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
                wordSpacing: 1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            )),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: style,
        onPressed: onPressed,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [kcStartGradient, kcEndGradient],
              )),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                text ?? "",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
              ),
              smallHorizontalSpace,
              const Icon(
                Icons.arrow_forward_ios_rounded,
                color: Colors.white,
                size: 18,
              )
            ],
          ),
        ));
  }
}
