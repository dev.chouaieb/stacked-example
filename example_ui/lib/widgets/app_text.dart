import 'package:example_ui/shared/app_colors.dart';
import 'package:example_ui/shared/styles.dart';
import 'package:flutter/material.dart';

class AppText extends StatelessWidget {
  final String? text;
  final TextStyle style;
  final TextAlign alignment;

  const AppText.headingOne(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = heading1Style,
        alignment = align,
        super(key: key);
  AppText.headingTwo(this.text,
      {Key? key,
      Color color = kcPrimaryColor,
      TextAlign align = TextAlign.start})
      : style = heading2Style.copyWith(color: color),
        alignment = align,
        super(key: key);

  const AppText.headingThree(this.text,
      {Key? key, TextAlign align = TextAlign.start})
      : style = heading3Style,
        alignment = align,
        super(key: key);

  AppText.body(this.text,
      {Key? key, Color color = kcGreyColor, TextAlign align = TextAlign.start})
      : style = bodyStyle.copyWith(color: color),
        alignment = align,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text ?? "",
      style: style,
      textAlign: alignment,
    );
  }
}
