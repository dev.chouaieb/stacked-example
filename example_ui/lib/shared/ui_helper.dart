import 'package:flutter/material.dart';

const Widget tinyHorizontalSpace = SizedBox(width: 5.0);
const Widget smallHorizontalSpace = SizedBox(width: 10.0);
const Widget regularHorizontalSpace = SizedBox(width: 18.0);
const Widget normalHorizontalSpace = SizedBox(width: 25.0);
const Widget mediumHorizontalSpace = SizedBox(width: 35);
const Widget largeHorizontalSpace = SizedBox(width: 50.0);

const Widget tinyVerticalSpace = SizedBox(height: 5.0);
const Widget smallVerticalSpace = SizedBox(height: 10.0);
const Widget regularVerticalSpace = SizedBox(height: 18.0);
const Widget normalVerticalSpace = SizedBox(height: 25.0);
const Widget mediumVerticalSpace = SizedBox(height: 32.0);
const Widget largeVerticalSpace = SizedBox(height: 50.0);
