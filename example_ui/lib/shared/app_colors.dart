import 'package:flutter/material.dart';

const Color kcPrimaryColor = Color(0xffED4040);
const Color kcBlackColor = Color(0xff2C2B36);
const Color kcGreyColor = Color(0xff4E4B61);
const Color kcStartGradient = Color(0xffB61E1E);
const Color kcEndGradient = Color(0xffF94646);
