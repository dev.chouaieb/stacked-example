import 'package:example_ui/example_ui.dart';
import 'package:flutter/material.dart';

const TextStyle heading1Style = TextStyle(
  color: kcBlackColor,
  fontSize: 20,
  fontWeight: FontWeight.w700,
);

const TextStyle heading2Style = TextStyle(
  color: kcPrimaryColor,
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

const TextStyle heading3Style = TextStyle(
  color: kcBlackColor,
  fontSize: 16,
  fontWeight: FontWeight.w600,
);

const TextStyle bodyStyle = TextStyle(
  color: kcGreyColor,
  fontSize: 13,
  fontWeight: FontWeight.w400,
);
