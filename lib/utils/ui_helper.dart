import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helper {
  static InputDecoration decorate({String? hintText}) {
    return InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: hintText,
        
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
          borderSide: BorderSide(
            color: Colors.green,
            width: 1.0,
          ),
        ),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
          borderSide: BorderSide(
            color: Colors.grey,
            width: 1.0,
          ),
        ));
  }
}
