import 'package:dio/dio.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_example/data/data_source/remote/driver_response.dart';
import 'package:stacked_example/data/repository/driver_repository.dart';
import 'package:stacked_example/di/locator.dart';

class DriverViewModel extends BaseViewModel {
  final repo = getIt<DriverRepository>();

  List<Driver> drivers = List.empty(growable: true);

  init() async {
    setBusyForObject(null, true);

    try {
      drivers = await repo.getAllDrivers();
    } on Exception catch (exception) {
      if (exception is DioError) {
        setError(exception.message);
      } else {
        setError("Unknown API error");
      }
    } finally {
      setBusyForObject(null, false);
    }
  }
}
