import 'package:example_ui/example_ui.dart';
import 'package:example_ui/shared/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_example/ui/driver/driver_item.dart';
import 'package:stacked_example/ui/driver/driver_viewmodel.dart';

class DriverView extends StatelessWidget {
  final String firstName;
  final String lastName;
  const DriverView({Key? key, required this.firstName, required this.lastName})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DriverViewModel>.reactive(
      onModelReady: (viewModel) => viewModel.init(),
      builder: (context, viewModel, child) => Scaffold(
          body: Column(children: [
        ClipPath(
          clipper: MyCustomClipper(),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            color: kcPrimaryColor,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const CircleAvatar(
                    radius: 40,
                    backgroundImage: AssetImage("assets/avatar.png"),
                  ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        AppText.headingTwo(
                          "Good morning,",
                          color: Colors.white,
                        ),
                        tinyVerticalSpace,
                        AppText.body(
                          "$firstName $lastName",
                          color: Colors.white,
                        )
                      ]),
                  largeHorizontalSpace
                ]),
          ),
        ),
        smallVerticalSpace,
        AppText.headingTwo("Top 20 ranking 2022"),
        tinyVerticalSpace,
        Expanded(
          child: viewModel.isBusy
              ? const Center(child: CircularProgressIndicator())
              : viewModel.hasError
                  ? Center(child: Text(viewModel.modelError.toString()))
                  : ListView.builder(
                      itemCount: viewModel.drivers.length,
                      itemBuilder: ((context, index) => Card(
                            elevation: 3,
                            margin: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 20),
                            child: DriverItem(
                              item: viewModel.drivers[index],
                            ),
                          )),
                    ),
        ),
      ])),
      viewModelBuilder: () => DriverViewModel(),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  var radius = 25.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height);

    path.arcToPoint(Offset(radius, size.height - radius),
        radius: Radius.circular(radius));

    path.lineTo(size.width - radius, size.height - radius);
    path.arcToPoint(Offset(size.width, size.height),
        radius: Radius.circular(radius));

    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
