import 'package:example_ui/example_ui.dart';
import 'package:flutter/material.dart';
import 'package:stacked_example/data/data_source/remote/driver_response.dart';

class DriverItem extends StatelessWidget {
  final Driver item;
  const DriverItem({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      leading: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppText.headingTwo(
            item.position.toString(),
          ),
          normalHorizontalSpace,
          const CircleAvatar(
            radius: 30,
            backgroundImage: AssetImage("assets/avatar.png"),
          ),
        ],
      ),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText.headingThree(item.driverName),
          AppText.body(item.teamName)
        ],
      ),
      horizontalTitleGap: 20,
    );
  }
}
