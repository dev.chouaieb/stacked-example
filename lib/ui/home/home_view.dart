import 'package:example_ui/example_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_example/di/locator.dart';
import 'package:stacked_example/ui/home/home_viewmodel.dart';
import 'package:stacked_example/utils/ui_helper.dart';

class HomeView extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final firstNameController = TextEditingController()..text = "Jonah";
  final lastNameController = TextEditingController();
  HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      builder: (context, viewModel, child) => SafeArea(
          child: Material(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/red_logo.png"),
                    normalVerticalSpace,
                    const AppText.headingOne("Lets Get Familiar"),
                    tinyVerticalSpace,
                    AppText.body("Introduce Yourself")
                  ]),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 35.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.body("First Name"),
                          TextFormField(
                            controller: firstNameController,
                            decoration: Helper.decorate(
                                hintText: "Type in your first name"),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp('[a-zA-Z]')),
                            ],
                            validator: validateName,
                          ),
                          regularVerticalSpace,
                          AppText.body("Last Name"),
                          TextFormField(
                            controller: lastNameController,
                            decoration: Helper.decorate(
                                hintText: "Type in your last name"),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp('[a-zA-Z]')),
                            ],
                            validator: validateName,
                          ),
                        ],
                      ),
                    ),
                  ),
                  largeVerticalSpace,
                  AppButton.primary(
                    text: "Next",
                    onPressed: () {
                      if (_formKey.currentState?.validate() ?? false) {
                        viewModel.navigateToNextPage(firstNameController.text ,lastNameController.text);
                      }
                    },
                  )
                ],
              ),
            )
          ],
        ),
      )),
      viewModelBuilder: () => getIt<HomeViewModel>(),
    );
  }

  String? validateName(String? text) {
    if (text == null || text.isEmpty) {
      return 'Name is empty';
    }
    return null;
  }
}
