import 'package:injectable/injectable.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_example/core/app.router.dart';
import 'package:stacked_services/stacked_services.dart';
@injectable
class HomeViewModel extends BaseViewModel {
  final NavigationService navigationService;
  HomeViewModel(this.navigationService);

  navigateToNextPage(firstName,lastName) {
    navigationService.navigateTo(Routes.driverView,
        arguments:
            DriverViewArguments(firstName :firstName ,lastName :lastName));
  }
}
