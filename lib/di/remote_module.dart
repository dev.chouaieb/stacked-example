import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked_example/data/data_source/local/driver_dao.dart';
import 'package:stacked_example/data/data_source/remote/api/driver_api.dart';
import 'package:stacked_example/data/repository/driver_repository.dart';

@module
abstract class RemoteModule {
  @lazySingleton
  Dio get dio => Dio();

  DriverApi provideDriverApi(Dio dio) => DriverApi(dio);

  DriverRepository provideDriverRepository(DriverApi api ,DriverDao dao) => DriverRepository(api,dao);
}
