// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:stacked_services/stacked_services.dart' as _i8;

import '../data/data_source/local/app_database.dart' as _i3;
import '../data/data_source/local/driver_dao.dart' as _i6;
import '../data/data_source/remote/api/driver_api.dart' as _i5;
import '../data/repository/driver_repository.dart' as _i7;
import '../ui/home/home_viewmodel.dart' as _i9;
import 'cache_module.dart' as _i10;
import 'remote_module.dart' as _i11;
import 'service_module.dart' as _i12; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final cacheModule = _$CacheModule();
  final remoteModule = _$RemoteModule();
  final servicesModule = _$ServicesModule();
  await gh.factoryAsync<_i3.AppDatabase>(() => cacheModule.appDatabase,
      preResolve: true);
  gh.lazySingleton<_i4.Dio>(() => remoteModule.dio);
  gh.factory<_i5.DriverApi>(
      () => remoteModule.provideDriverApi(get<_i4.Dio>()));
  gh.factory<_i6.DriverDao>(
      () => cacheModule.prepareDriverDao(get<_i3.AppDatabase>()));
  gh.factory<_i7.DriverRepository>(() => remoteModule.provideDriverRepository(
      get<_i5.DriverApi>(), get<_i6.DriverDao>()));
  gh.lazySingleton<_i8.NavigationService>(
      () => servicesModule.navigationService);
  gh.factory<_i9.HomeViewModel>(
      () => _i9.HomeViewModel(get<_i8.NavigationService>()));
  return get;
}

class _$CacheModule extends _i10.CacheModule {}

class _$RemoteModule extends _i11.RemoteModule {}

class _$ServicesModule extends _i12.ServicesModule {
  @override
  _i8.NavigationService get navigationService => _i8.NavigationService();
}
