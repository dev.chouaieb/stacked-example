import 'package:injectable/injectable.dart';
import 'package:stacked_example/data/data_source/local/app_database.dart';
import 'package:stacked_example/data/data_source/local/driver_dao.dart';

@module
abstract class CacheModule {
  @preResolve
  Future<AppDatabase> get appDatabase =>
      $FloorAppDatabase.databaseBuilder("app_database.db").build();

  DriverDao prepareDriverDao(AppDatabase appDatabase) => appDatabase.driverDao;
}
