import 'package:injectable/injectable.dart';
import 'package:get_it/get_it.dart';

import 'locator.config.dart';

final GetIt getIt = GetIt.instance;

@injectableInit
Future setupLocator() async => 
    $initGetIt(getIt);
