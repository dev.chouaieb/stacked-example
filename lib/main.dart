import 'package:example_ui/example_ui.dart';
import 'package:flutter/material.dart';
import 'package:stacked_example/core/app.router.dart';
import 'package:stacked_example/di/locator.dart';
import 'package:stacked_services/stacked_services.dart';

void main() async{
   WidgetsFlutterBinding.ensureInitialized();
 await setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: kcPrimaryColor,
      ),
      navigatorKey: StackedService.navigatorKey,
      onGenerateRoute: StackedRouter().onGenerateRoute,
    );
  }
}
