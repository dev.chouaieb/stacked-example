import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_example/ui/home/home_view.dart';
import 'package:stacked_example/ui/driver/driver_view.dart';

@StackedApp(
  routes: [
    MaterialRoute(page: HomeView, initial: true),
    MaterialRoute(page: DriverView)
  ],
)
class AppSetup {}
