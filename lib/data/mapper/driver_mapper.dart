import 'package:stacked_example/data/data_source/local/driver_entity.dart';
import 'package:stacked_example/data/data_source/remote/driver_response.dart';

extension DriverExt on Driver {
  DriverEntity toEntity() => DriverEntity(
        position,
        driverName,
        teamName,
        points,
        nationality,
      );
}

extension DriverEntityExt on DriverEntity {
  Driver toModel() => Driver(
        position,
        driverName,
        teamName,
        nationality,
        points,
      );
}
