import 'package:floor/floor.dart';

import 'package:stacked_example/data/data_source/local/driver_entity.dart';

const _tableName = 'drivers';

@dao
abstract class DriverDao {

  @Query('select * FROM $_tableName ')
  Future<List<DriverEntity>> getAllDrivers();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertAll(List<DriverEntity> entities);

}
