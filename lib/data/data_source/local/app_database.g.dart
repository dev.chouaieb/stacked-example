// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  DriverDao? _driverDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `DriverEntity` (`position` INTEGER NOT NULL, `driverName` TEXT NOT NULL, `teamName` TEXT NOT NULL, `points` TEXT NOT NULL, `nationality` TEXT NOT NULL, PRIMARY KEY (`position`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  DriverDao get driverDao {
    return _driverDaoInstance ??= _$DriverDao(database, changeListener);
  }
}

class _$DriverDao extends DriverDao {
  _$DriverDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _driverEntityInsertionAdapter = InsertionAdapter(
            database,
            'DriverEntity',
            (DriverEntity item) => <String, Object?>{
                  'position': item.position,
                  'driverName': item.driverName,
                  'teamName': item.teamName,
                  'points': item.points,
                  'nationality': item.nationality
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<DriverEntity> _driverEntityInsertionAdapter;

  @override
  Future<List<DriverEntity>> getAllDrivers() async {
    return _queryAdapter.queryList('select * FROM drivers',
        mapper: (Map<String, Object?> row) => DriverEntity(
            row['position'] as int,
            row['driverName'] as String,
            row['teamName'] as String,
            row['points'] as String,
            row['nationality'] as String));
  }

  @override
  Future<void> insertAll(List<DriverEntity> entities) async {
    await _driverEntityInsertionAdapter.insertList(
        entities, OnConflictStrategy.replace);
  }
}
