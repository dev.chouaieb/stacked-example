import 'package:injectable/injectable.dart';
import 'package:stacked_example/data/data_source/local/driver_dao.dart';
import 'package:stacked_example/data/data_source/local/driver_entity.dart';
// required package imports
import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
part 'app_database.g.dart';


@Database(version: 1, entities: [DriverEntity])
abstract class AppDatabase extends FloorDatabase {
  DriverDao get driverDao;

}
