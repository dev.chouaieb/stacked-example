import 'package:floor/floor.dart';
@entity
class DriverEntity {
  @primaryKey
  final int position;
  final String driverName;
  final String teamName;
  final String points;
  final String nationality;

  DriverEntity(
    this.position,
    this.driverName,
    this.teamName,
    this.points,
    this.nationality,
  );
}
