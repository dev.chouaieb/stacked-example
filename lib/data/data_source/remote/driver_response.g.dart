// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DriverResponse _$DriverResponseFromJson(Map<String, dynamic> json) =>
    DriverResponse(
      (json['results'] as List<dynamic>)
          .map((e) => Driver.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DriverResponseToJson(DriverResponse instance) =>
    <String, dynamic>{
      'results': instance.drivers,
    };

Driver _$DriverFromJson(Map<String, dynamic> json) => Driver(
      json['position'] as int,
      json['driver_name'] as String,
      json['team_name'] as String,
      json['nationality'] as String,
      json['points'] as String,
    );

Map<String, dynamic> _$DriverToJson(Driver instance) => <String, dynamic>{
      'position': instance.position,
      'driver_name': instance.driverName,
      'team_name': instance.teamName,
      'nationality': instance.nationality,
      'points': instance.points,
    };
