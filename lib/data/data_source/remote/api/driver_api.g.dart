// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _DriverApi implements DriverApi {
  _DriverApi(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://formula-1-standings.p.rapidapi.com/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<DriverResponse> getDrivers() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{
      r'X-RapidAPI-Key': 'cf13628089mshf75574dba31d9a2p10ada5jsna2be4e0c9555',
      r'X-RapidAPI-Host': 'formula-1-standings.p.rapidapi.com'
    };
    _headers.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DriverResponse>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, 'standings/drivers',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DriverResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
