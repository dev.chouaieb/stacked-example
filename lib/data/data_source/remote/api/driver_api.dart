import 'package:dio/dio.dart' hide Headers;

import 'package:retrofit/http.dart';

import 'package:stacked_example/data/data_source/remote/driver_response.dart';

part 'driver_api.g.dart';

@RestApi(baseUrl: "https://formula-1-standings.p.rapidapi.com/")
abstract class DriverApi {
  factory DriverApi(Dio dio, {String baseUrl}) = _DriverApi;
  @GET("standings/drivers")
  @Headers(<String, dynamic>{
    'X-RapidAPI-Key': 'cf13628089mshf75574dba31d9a2p10ada5jsna2be4e0c9555',
    'X-RapidAPI-Host': 'formula-1-standings.p.rapidapi.com'
  })
  Future<DriverResponse> getDrivers();
}
