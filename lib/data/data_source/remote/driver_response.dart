import 'package:json_annotation/json_annotation.dart';

part 'driver_response.g.dart';

@JsonSerializable()
class DriverResponse {
  @JsonKey(name: "results")
  final List<Driver> drivers;

  DriverResponse(this.drivers);

   factory DriverResponse.fromJson(Map<String, dynamic> json) => _$DriverResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DriverResponseToJson(this);
}

@JsonSerializable()
class Driver {
  final int position;
  @JsonKey(name: "driver_name")
  final String driverName;
  @JsonKey(name: "team_name")
  final String teamName;
  final String nationality;
  final String points;

  Driver(
    this.position,
    this.driverName,
    this.teamName,
    this.nationality,
    this.points,
  );

   factory Driver.fromJson(Map<String, dynamic> json) =>
      _$DriverFromJson(json);

  Map<String, dynamic> toJson() => _$DriverToJson(this);
}
