
import 'package:stacked_example/data/data_source/local/driver_dao.dart';
import 'package:stacked_example/data/data_source/remote/api/driver_api.dart';
import 'package:stacked_example/data/data_source/remote/driver_response.dart';
import 'package:stacked_example/data/mapper/driver_mapper.dart';

class DriverRepository {
  final DriverApi api;
  final DriverDao dao;

  DriverRepository(this.api, this.dao);

  Future<List<Driver>> getAllDrivers() async {
    var response = await api.getDrivers();
    await dao.insertAll(response.drivers.map((e) => e.toEntity()).toList());
    return response.drivers;
  }

 
}
